// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "tdsGameMode.generated.h"

UCLASS(minimalapi)
class AtdsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AtdsGameMode();
};



