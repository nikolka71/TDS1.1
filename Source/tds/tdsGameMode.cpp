// Copyright Epic Games, Inc. All Rights Reserved.

#include "tdsGameMode.h"
#include "tdsPlayerController.h"
#include "tdsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AtdsGameMode::AtdsGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AtdsPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}